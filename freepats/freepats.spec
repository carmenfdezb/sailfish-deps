# Based on timidity-freepats.spec from altlinux

Name: freepats
Version: 20060219
Release: 1

Summary: A set of sound fonts for use in audio synths
License: GPLv2+
Group: Sound

Url: http://freepats.zenvoid.org
Source: %{url}/freepats-%{version}.tar.xz

BuildArch: noarch

%description
FreePats is a set of sound fonts for use in audio synths.

FreePats project is to create a free and open set of GUS compatible
patches that can be used with softsynths such as TiMidity and WildMidi.

%prep
%setup -n %{name}

%build

%install
# Patches
install -d %{buildroot}%{_datadir}/midi/freepats/Drum_000
install -d %{buildroot}%{_datadir}/midi/freepats/Tone_000
install -m 644 Drum_000/* %{buildroot}%{_datadir}/midi/freepats/Drum_000
install -m 644 Tone_000/* %{buildroot}%{_datadir}/midi/freepats/Tone_000

# Config
install -d %{buildroot}%{_sysconfdir}/timidity/

cat - freepats.cfg > %{buildroot}%{_sysconfdir}/timidity/freepats.cfg << _EOF_
dir %{_datadir}/midi/freepats

_EOF_

%files
%doc COPYING README
%{_sysconfdir}/timidity
%{_datadir}/midi/freepats
