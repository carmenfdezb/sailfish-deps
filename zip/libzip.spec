# 
# Do NOT Edit the Auto-generated Part!
# Generated by: spectacle version 0.32
# 

Name:       libzip

# >> macros
# << macros

Summary:    C library for reading, creating, and modifying zip archives
Version:    1.8.0
Release:    1
Vendor:     meego
Group:      System/Libraries
License:    BSD
URL:        http://www.nih.at/libzip/index.html
Source0:    http://libzip.org/download/%{name}-%{version}.tar.xz
Source100:  libzip.yaml
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
BuildRequires:  pkgconfig(zlib) >= 1.1.2
BuildRequires:  cmake

%description
libzip is a C library for reading, creating, and modifying zip archives. Files
can be added from data buffers, files, or compressed data copied directly from 
other zip archives. Changes made without closing the archive can be reverted. 
The API is documented by man pages.


%package devel
Summary:    Development files for %{name}
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}

%description devel
The libzip-devel package contains libraries and header files for
developing applications that use %{name}.


%package -n zip
Summary:    Command-line tools for %{name}
Group:      Applications/Archiving
Requires:   %{name} = %{version}-%{release}

%description -n zip
This package contains command-line tools that use %{name}.


%prep
%setup -q -n %{name}-%{version}

# >> setup
# << setup

%build
# >> build pre
# << build pre

%cmake .  \
    -DENABLE_COMMONCRYPTO=OFF \
    -DENABLE_GNUTLS=OFF \
    -DENABLE_MBEDTLS=OFF \
    -DENABLE_OPENSSL=OFF \
    -DENABLE_WINDOWS_CRYPTO=OFF \
    -DENABLE_BZIP2=OFF \
    -DENABLE_LZMA=OFF \
    -DENABLE_ZSTD=OFF

make %{?_smp_mflags}

# >> build post
# << build post

%install
rm -rf %{buildroot}
# >> install pre
# << install pre
%make_install

# >> install post
# << install post

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/libzip.so.5*
# >> files
# << files

%files devel
%defattr(-,root,root,-)
%{_includedir}
%{_libdir}/libzip.so
%{_libdir}/pkgconfig
%{_libdir}/cmake
# >> files devel
# << files devel

%files -n zip
%defattr(-,root,root,-)
%{_bindir}
%{_datadir}/doc
# >> files zip
# << files zip
